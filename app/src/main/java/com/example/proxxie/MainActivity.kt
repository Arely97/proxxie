package com.example.proxxie

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val baseUrl =
        "https://thingspeak.com/channels/1289238/charts/1?bgcolor=%23FFFCE4&color=%23CB3234&dynamic=true&results=60&title=Aproximación de objetos&type=spline"
    private val baseUrl1 =
        "https://thingspeak.com/channels/1289238/charts/2?bgcolor=%23FFFCE4&color=%23CB3234&dynamic=true&results=60&title=Aproximación de objetos&type=spline"
    private val baseUrl2 =
        "https://thingspeak.com/channels/1289238/charts/3?bgcolor=%23FFFCE4&color=%23CB3234&dynamic=true&results=60&title=Aproximación de objetos&type=spline"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        swipeRefresh.setOnRefreshListener {
            webView.reload()
            webView1.reload()
            webView2.reload()
        }

        val myWebView: WebView = findViewById(R.id.webView)
        webView.webViewClient = WebViewClient()
        val settings = webView.settings
        settings.javaScriptEnabled = true
        myWebView.loadUrl(baseUrl)


        val myWebView1: WebView = findViewById(R.id.webView1)
        webView1.webViewClient = WebViewClient()
        val settings1 = webView1.settings
        settings1.javaScriptEnabled = true
        myWebView1.loadUrl(baseUrl1)


        val myWebView2: WebView = findViewById(R.id.webView2)
        webView2.webViewClient = WebViewClient()
        val settings2 = webView2.settings
        settings2.javaScriptEnabled = true
        myWebView2.loadUrl(baseUrl2)

        webView.webChromeClient = object : WebChromeClient() {

        }
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                resquest: WebResourceRequest?
            ): Boolean {
                return false
            }


            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                swipeRefresh.isRefreshing = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                swipeRefresh.isRefreshing = false
            }
        }
        webView1.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                resquest: WebResourceRequest?
            ): Boolean {
                return false
            }


            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                swipeRefresh.isRefreshing = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                swipeRefresh.isRefreshing = false
            }
        }
        webView2.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                resquest: WebResourceRequest?
            ): Boolean {
                return false
            }


            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                swipeRefresh.isRefreshing = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                swipeRefresh.isRefreshing = false
            }
        }
        todo.setOnClickListener {
            val url = "https://thingspeak.com/channels/1289238/feed.csv"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        recargar.setOnClickListener {
           webView.reload()
           webView1.reload()
           webView2.reload()
       }
       tab1.setOnClickListener {
           val url = "https://thingspeak.com/channels/1289238/field/1.csv"
           val i = Intent(Intent.ACTION_VIEW)
           i.data = Uri.parse(url)
           startActivity(i)
       }
       tab2.setOnClickListener {
           val url = "https://thingspeak.com/channels/1289238/field/2.csv"
           val i = Intent(Intent.ACTION_VIEW)
           i.data = Uri.parse(url)
           startActivity(i)
       }
       tab3.setOnClickListener {
           val url = "https://thingspeak.com/channels/1289238/field/3.csv"
           val i = Intent(Intent.ACTION_VIEW)
           i.data = Uri.parse(url)
           startActivity(i)
       }
           }

}